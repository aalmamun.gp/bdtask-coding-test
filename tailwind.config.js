/** @type {import('tailwindcss').Config} */

const { fontFamily } = require("tailwindcss/defaultTheme");

export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    screens: {
      sm: "600px",
      // => @media (min-width: 600px) { ... }

      md: "768px",
      // => @media (min-width: 768px) { ... }

      lg: "1024px",
      // => @media (min-width: 1024px) { ... }

      xl: "1280px",
      // => @media (min-width: 1280px) { ... }

      "2xl": "1536px",
      // => @media (min-width: 1536px) { ... }

      "3xl": "1680px",
      // => @media (min-width: 1600px) { ... }
    },
    extend: {
      fontFamily: {
        sans: ["Lato", ...fontFamily.sans],
      },
      colors: {
        dark: {
          primary: "#15202f",
          secondary: "#2b3037",
          tertiary: "#141c26",
        },
        green: {
          primary: "#00a653",
          secondary: "#34d399",
        },
        cyan: "#22d3ee",
        purple: {
          primary: "#5a1c8e",
          secondary: "#7b22c9",
          light: "#d8b4fe",
        },
        red: {
          light: "#fca5a5",
        },
      },
    },
  },
  variants: {},
  plugins: [],
};
