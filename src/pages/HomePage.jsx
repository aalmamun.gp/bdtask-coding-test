import BuySell from "../components/BuySell";
import Footer from "../components/Footer";
import GraphChart from "../components/GraphChart";
import SellTable2 from "../components/SellTable2";
import TopNav from "../components/TopNav";
import TradingList from "../components/TradingList";

const HomePage = () => {
  return (
    <>
      <TopNav />
      <div className="md:px-4 grid grid-cols-1 lg:grid-cols-2 xl:grid-cols-4 gap-3">
        <div className="xl:col-span-2">
          <TradingList />
        </div>
        <BuySell />
        <SellTable2 />
      </div>
      <div className="grid lg:grid-cols-2">
        <GraphChart />
      </div>
      <Footer />
    </>
  );
};

export default HomePage;
