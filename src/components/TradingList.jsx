import { FullScren, Setting2, Connected, Download, Close } from "./ALL-IMAGES";
import { TradingListData } from "../data/data";
const TradingList = () => {
  return (
    <div className="bg-dark-primary w-full py-3">
      <div className="flex justify-between md:items-center flex-col md:flex-row px-4 md:px-0">
        <div className="mb-3 md:mb-0">
          <button
            type="button"
            className="active_btn_primary py-1 px-4 text-white me-1"
          >
            Teading List
          </button>
          <button
            type="button"
            className="btn_primary py-1 px-4 text-white relative after:w-1 after:rounded-sm after:h-full after:bg-white after:top-0 after:right-0 after:absolute"
          >
            Watch List
          </button>
        </div>
        <div className="flex gap-2">
          <button
            type="button"
            className="bg-black py-1 px-2 pr-10 text-gray-400 text-sm"
          >
            Instruments
          </button>
          <button
            type="button"
            className="text-white bg-gradient-to-r from-cyan to-green-400 w-8 h-8 rounded-md p-1"
          ></button>
          <button
            type="button"
            className="text-white bg-gradient-to-r from-purple-700 to-purple-500 w-8 h-8 rounded-md p-1"
          >
            <img className="w-full h-full object-cover" src={Download} alt="" />
          </button>
          <button
            type="button"
            className="text-white bg-gradient-to-r from-purple-700 to-purple-500 w-8 h-8 rounded-md p-1"
          >
            <img className="w-full h-full object-cover" src={Setting2} alt="" />
          </button>
          <button
            type="button"
            className="text-white bg-gradient-to-r from-purple-700 to-purple-500 w-8 h-8 rounded-md p-1"
          >
            <img
              className="w-full h-full object-cover"
              src={Connected}
              alt=""
            />
          </button>
          <button
            type="button"
            className="text-white bg-gradient-to-r from-purple-700 to-purple-500 w-8 h-8 rounded-md p-1"
          >
            <img
              className="w-full h-full object-cover"
              src={FullScren}
              alt=""
            />
          </button>
          <button
            type="button"
            className="text-white bg-gradient-to-r from-purple-700 to-purple-500 w-8 h-8 rounded-md p-2"
          >
            <img className="w-full h-full object-cover" src={Close} alt="" />
          </button>
        </div>
      </div>
      <div className="max-h-96 w-full overflow-y-scroll p-2 mt-4">
        <table className="table-auto overflow-scroll text-white text-sm w-full">
          <thead className="bg-black">
            <tr>
              <th className="p-2">Id</th>
              <th className="p-2">Short Name</th>
              <th className="p-2 ">Group</th>
              <th className="p-2">BQ</th>
              <th className="p-2">Bid</th>
              <th className="p-2">Ask</th>
              <th className="p-2">AQ</th>
              <th className="p-2">Last</th>
              <th className="p-2">Open</th>
              <th className="p-2">High</th>
            </tr>
          </thead>
          <tbody>
            {TradingListData.map((items, index) => {
              const { id, name, group, bq, bid, ask, aq, last, open, high } =
                items;
              return (
                <tr key={index}>
                  <td className="py-2 px-4">{id}</td>
                  <td className="py-2 px-4">{name}</td>
                  <td className="text-cyan py-2 px-4">{group}</td>
                  <td className="py-2 px-4">{bq}</td>
                  <td className="py-2 px-4">{bid}</td>
                  <td className="py-2 px-4">{ask}</td>
                  <td className="py-2 px-4">{aq}</td>
                  <td className="text-gray-500 py-2 px-4">{last}</td>
                  <td className="text-blue-800 py-2 px-4">{open}</td>
                  <td className="py-2 px-4">{high}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default TradingList;
