const BuySell = () => {
  return (
    <div className="bg-dark-primary w-full overflow-hidden">
      <div className="flex justify-between items-center p-2">
        <h2 className="text-white font-bold text-md">Buy-Sell</h2>
        <form className="w-[200px]">
          <div className="relative  text-gray-300 bg-transparent">
            <select
              className="appearance-none w-full py-1 px-2 bg-transparent border rounded-sm"
              name="whatever"
              id="frm-whatever"
            >
              <option className="text-dark-primary" value="">
                All Client
              </option>
              <option className="text-dark-primary" value="1">
                Item 1
              </option>
              <option className="text-dark-primary" value="2">
                Item 2
              </option>
              <option className="text-dark-primary" value="3">
                Item 3
              </option>
            </select>
            <div className="pointer-events-none absolute right-0 top-0 bottom-0 flex items-center px-2 text-gray-700 border-l">
              <svg
                className="h-4 w-4"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path
                  fill="white"
                  d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"
                />
              </svg>
            </div>
          </div>
        </form>
      </div>
      <div className="w-full overflow-scroll px-2">
        <table className="table-auto overflow-scroll text-white text-sm w-full">
          <thead className="bg-black text-left">
            <tr className="">
              <th className="p-2">
                <span className="pr-2">Open :</span> 52.2
              </th>
              <th className="p-2">
                <span className="pr-2">High :</span> 522
              </th>
              <th className="p-2">
                <span className="pr-2">Low :</span> 522
              </th>
            </tr>
            <tr className="">
              <th className="p-2">
                <span className="pr-2">DH :</span> 52.2
              </th>
              <th className="p-2">
                <span className="pr-2">Chg% :</span> 522
              </th>
              <th className="p-2">
                <span className="pr-2">Val :</span> 522
              </th>
            </tr>
          </thead>
        </table>
      </div>
      <div className="h-[300px] overflow-scroll bg-[#0b351d]">
        <table className="table-auto overflow-scroll text-white text-sm w-full ">
          <thead className="bg-black text-left">
            <tr className="bg-dark-primary">
              <th className="p-2">Orders #</th>
              <th className="p-2">CumQ</th>
              <th className="p-2">Quantity</th>
              <th className="p-2">Quantity</th>
              <th className="p-2">Bid</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  );
};

export default BuySell;
