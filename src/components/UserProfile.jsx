import { User } from "./ALL-IMAGES";

const UserProfile = () => {
  return (
    <div className="absolute top-[100px] md:top-11 right-2 z-50 bg-dark-primary w-full max-w-sm p-6  rounded-md border">
      <div className="border-b-[1px] border-gray-500 flex justify-between items-center text-white pb-6">
        <img className="w-16 rounded-md" src={User} alt="User" />
        <div>
          <div className="flex">
            <h2 className="font-bold text-lg">mmrahmad1</h2>
            <span className="px-2 bg-green-400 text-white rounded-lg ms-2">
              Pro
            </span>
          </div>
          <p className="text-sm text-gray-400">ketesim3062gmail.com</p>
        </div>
      </div>
      <ul className="text-gray-400 mt-6 border-b-[1px] border-gray-500">
        <li className="mb-4">Login As mmrahmad1</li>
        <li className="mb-4">Need a BO Account</li>
        <li className="mb-4">My Profile</li>
      </ul>
      <ul className="text-gray-400 mt-6">
        <li className="mb-4">Account Settings</li>
        <li className="mb-4">Sign Out</li>
      </ul>
    </div>
  );
};

export default UserProfile;
2;
