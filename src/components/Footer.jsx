const Footer = () => {
  return (
    <div className="bg-dark-primary px-4 py-8 flex justify-center items-center w-full mt-4">
      <p className="text-gray-400 text-base">
        2023 © &nbsp;<span className="text-cyan">Benemoy</span>
      </p>
    </div>
  );
};

export default Footer;
