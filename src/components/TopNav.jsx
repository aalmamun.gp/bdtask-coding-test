import { useState } from "react";
import {
  BrandLogo,
  DarkLight,
  Menu,
  Message,
  Setting,
  User,
} from "./ALL-IMAGES";
import UserProfile from "./UserProfile";

const TopNav = () => {
  const [showProfile, setShowProfile] = useState(false);
  const userProfileOpen = () => {
    setShowProfile(!showProfile);
  };
  return (
    <>
      <div className="flex flex-col md:flex-row justify-between md:items-center bg-dark-primary px-4 gap-4 mb-3">
        <a href="#">
          <img className="w-auto h-[40px] max-h-full" src={BrandLogo} alt="" />
        </a>
        <div className="flex gap-1">
          <div>
            <h2 className="text-white font-bold text-2xl">12:45:42</h2>
          </div>
          <button type="button" className="w-8 h-8 p-1">
            <img
              className="w-full h-full object-cover"
              src={DarkLight}
              alt=""
            />
          </button>
          <button type="button" className="w-8 h-8 p-1">
            <img className="w-full h-full object-cover" src={Setting} alt="" />
          </button>
          <button type="button" className="w-8 h-8 p-1">
            <img className="w-full h-full object-cover" src={Menu} alt="" />
          </button>
          <button type="button" className="w-8 h-8 p-1">
            <img className="w-full h-full object-cover" src={Message} alt="" />
          </button>
          <button
            onClick={userProfileOpen}
            type="button"
            className="w-8 h-8  rounded-md overflow-hidden"
          >
            <img className="w-full h-full object-cover" src={User} alt="" />
          </button>
        </div>
      </div>
      {showProfile && <UserProfile />}
    </>
  );
};

export default TopNav;
